package cn.langpy.simforkjoin.core;

import org.aspectj.lang.ProceedingJoinPoint;

import java.util.List;
import java.util.concurrent.Callable;


public class CutTask implements Callable<List> {
    private ProceedingJoinPoint joinPoint;
    private Object[] args;

    public CutTask(ProceedingJoinPoint joinPoint, Object[] args, int start, int end) {
        int argsLen = args.length;
        this.args = new Object[argsLen];
        this.joinPoint = joinPoint;
        List arg = (List) args[0];
        List subList = arg.subList(start, end);
        this.args[0] = subList;
        for (int i = 1; i < argsLen; i++) {
            this.args[i] = args[i];
        }
    }

    public List call() {
        try {
            return (List) joinPoint.proceed(args);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }
}
