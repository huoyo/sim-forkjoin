package cn.langpy.simforkjoin.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

@Configuration
public class InitConfig {
    @Bean("defaultExecutor")
    public ExecutorService defaultExecutor() {
        return ForkJoinPool.commonPool();
    }
}
