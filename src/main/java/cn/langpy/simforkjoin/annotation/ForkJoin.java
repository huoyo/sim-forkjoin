package cn.langpy.simforkjoin.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ForkJoin {
    /**
     * the size of List parameter for every thread
     */
    int threshold() default 400;
    /**
     * the thread pool to process List
     */
    String executor() default "";

    /**
     * set it true when the method has return value
     */
    boolean isReturn() default true;
}
