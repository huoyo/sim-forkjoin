# simforkjoin

#### 介绍

simforkjoin是一个简易版的forkjoin并行任务注解，
用于解决springboot项目下对大量数据单线程处理的性能问题，通过一个注解将任务进行多线程拆分处理，以提高处理性能

> 注：并行并一定能提高处理速度，还取决于服务器的cpu以及其他IO损耗，要根据情况选择


#### 使用教程

1.  引入依赖
```
 <dependency>
    <groupId>cn.langpy</groupId>
    <artifactId>simforkjoin</artifactId>
    <version>1.0.1</version>
  </dependency>
```
2.  在需要并行化的方法加上注解@ForkJoin


加了注解以后，在执行的过程中，该方法会自动被拆分成多个任务执行，而不需要人为考虑任务的拆分以及合并结果的过程

```java
@Forkjoin
public List<T> test(List<T> param){
    ...
}
```

#### 注意事项

##### 1.  @Forkjoin的内部参数

######  1.1 threshold

> threshold表示每个任务处理的数量，默认为400，小于threshold的数据将不会被拆分

###### 1.2 executor

> 使用自定义的线程池(spring bean的形式)，当默认的线程池无法满足需求的时候，可以自定义

###### 1.3 isReturn

> @Forkjoin修饰的方法是否有返回值，默认有返回

##### 2. @Forkjoin修饰的方法参数

###### 2.1 要求方法参数的第一个值为List，且不能为空


#### 应用场景

1. 比如有一个List类型的集合list，需要需要对list中的每个元素进行处理，然后返回处理以后的结果，但是由于list的size太大，处理速度慢，需要将list进行并行化处理，并将结果汇总，就可以使用

```java
@Forkjoin
public List<T> test(List<T> list){
    for(int i=0;i<list.size();i++){
        T t = list.get(i);
        /*其他操作*/
        list.set(i,t);
    }
    return list;
}
```

```java
@Autowired
TestService testService;

List list = .....
List newList = testService.test(list);

```

#### 版本说明

> V1.0-SNAPSHOT：
>

#### 特别说明

本项目不会像传统的Fork/join一样将子任务再进行拆分，比如有一1000条数据量的任务，按照threshold进行划分成多个任务以后不会再进行子任务拆分





